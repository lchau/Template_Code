#!/usr/bin/env python2

"""
    Author: Larry Chau <larry.chau@lumentum.com>
    Description: TCP Client Example.
    Because sockets are network buffers they return when a buffer is emptied or full. We have to resort to fixed
    strings, flagging, or framed with payload size.
"""

import socket
import time

TCP_HOST = '127.0.0.1'
TCP_PORT = 10001
TCP_BUFFER_SIZE = 20
TCP_ENCODING = 'utf-8'

class TCPClient:
    def __init__(self, host, port, encoding='utf-8', buf_size=20):
        self.host = host
        self.port = port
        self.encoding = encoding
        self.buf_size = buf_size # maybe do validation here in the future
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        return
    def open(self):
        self.sock.connect((self.host, self.port))
        self.sock.settimeout(1.0)
        return
    def close(self):
        self.sock.close()
    def send_and_receive(self, message):
        payload = (message.encode())
        self.sock.send(payload)
        response = self.sock.recv(self.buf_size)
        response = response.decode(self.encoding)
        return response
        
if __name__ == '__main__':
    client = TCPClient(TCP_HOST, TCP_PORT, TCP_ENCODING, TCP_BUFFER_SIZE)
    client.open()
    exit_flag = False
    while (not exit_flag) :
        text = raw_input(">")
        if not text:
            print "Invalid"
            continue
        if (text == 'exit'): break
        try:
            print client.send_and_receive(text)
        except socket.timeout:
            break
        except socket.error:
            break
        if (text == 'stop'): break
    client.close()

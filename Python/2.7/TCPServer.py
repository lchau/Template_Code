#!/usr/bin/env python2

"""
    Author: Larry Chau <larry.chau@lumentum.com>
    Description: TCP Server Example.
"""

import select
import socket
import time

TCP_HOST = '127.0.0.1'
TCP_PORT = 10001
TCP_BUFFER_SIZE = 20
TCP_ENCODING = 'utf-8'


class TCPServer:
    Max_Clients=1
    def __init__(self, host, port, encoding='utf-8', buf_size=20):
        self.host = host
        self.port = port
        self.encoding = encoding
        self.buf_size = buf_size  # maybe do validation here in the future
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setblocking(1)
        return
    def open(self):
        self.sock.bind((self.host, self.port))
        self.sock.listen(TCPServer.Max_Clients)
        return
    def close(self):
        return self.sock.close()
    def accept(self):
        return self.sock.accept()

if __name__ == '__main__':
    server = TCPServer(TCP_HOST, TCP_PORT, TCP_ENCODING, TCP_BUFFER_SIZE)
    server.open()

    while True:
        print 'Waiting for connection'
        client_socket, client_addr = server.accept()
        try:
            print('Client connected:'), client_addr

            while True:
                data = client_socket.recv(TCP_BUFFER_SIZE)
                print 'Received(', client_addr, ')', data
                if not data:
                    print 'No data received from ', client_addr, ', disconnecting'
                    break
                client_socket.send(data)
                if (data == 'stop'): break
            if (data == 'stop'): break
        finally:
            client_socket.close()




(* Author: Larry Chau <LarryMChau@gmail.com>
 * Hello world in COOL - a classic
 *)

--All functions must be inside a class.
class Main inherits IO { --Using standard I/O so we inherit IO

   -- class Main has a feature which is a method called main,
   -- which is the entry point of the executable.
   -- MethodName() : Return Type where SELF_TYPE=This
   main(): SELF_TYPE {
	out_string("Hello, World!\n")
   };

};

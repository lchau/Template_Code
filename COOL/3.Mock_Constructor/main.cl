(* Author: Larry Chau <LarryMChau@gmail.com>
 * COOL does not have a concept of constructors
 * simply use a method to initialize fields.
 *)

class Main {
    main(): Int {
        --"let" keyword allows us to initialize local variables
        --which must be followed by an expression block
        let dependee: Dependee <- new Dependee in {
            dependee.init("Hello, World!\n"); --initialize data
            dependee.display(); --reference still holds data
                                --proving this is on the heap
            0;
        }
    };
};

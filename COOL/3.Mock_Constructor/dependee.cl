(* Author: Larry Chau <LarryMChau@gmail.com>
 * The calling class which has the mock constructor.
 *)

class Dependee inherits IO {
    data : String; --Member of type string

    --Mock constructor
    init(str : String) : Dependee {
        {
            data <- str;
            self; --Return this
        }
    };

    display() : SELF_TYPE {
        out_string(data)
    };
};

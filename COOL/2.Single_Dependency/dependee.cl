(* Author: Larry Chau <LarryMChau@gmail.com>
 * The file that main.cl depends on.
 *)

class Dependee inherits IO {
    hello_world() : SELF_TYPE {
        -- Function call to print hello world
        out_string("Hello, World!\n")
    };
};

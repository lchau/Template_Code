(* Author: Larry Chau <LarryMChau@gmail.com>
 * Example of a single dependency in COOL done via a method call.
 *)

class Main {
    --Attributes and methods have different namespaces
    --main is a member of type Foo
    main : Dependee;

    --Method name : Return Type where SELF_TYPE=This
    main(): Int {
        { --Multiple statements must be inside a statement block
            main <- new Dependee; --Initialize attribute
            main.hello_world(); --Call function
            0; --Last statement is always the return
               --Return 0 as exit status?
        }
    };
};

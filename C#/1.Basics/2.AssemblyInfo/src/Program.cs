﻿/* Author: Larry Chau
 * AssemblyInfo tells us what information is packaged with the executable such
 * as the company name and creator. This can be viewed in Windows by right clicking
 * the executable and viewing properties or with any other program used to view the
 * metadata of a file.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AssemblyInfo {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Hello, World!");
            Console.ReadKey();
        }
    }
}

﻿/* Author: Larry Chau
 * Basic example of a single dependency in C# with a method call.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Single_Dependency {
    class Program {
        static void Main(string[] args) {
            Dependee.Method();
        }
    }
}

﻿/* Author: Larry Chau
 * Caller dependee to print hello world.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Single_Dependency {
    class Dependee {
        // static method means part of class defintion and not object
        public static void Method() {
            Console.WriteLine("Hello, World!");
            Console.ReadKey();
        }
    }
}

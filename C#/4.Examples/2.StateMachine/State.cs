﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirmwareTester.Misc {
    public class State {
        public string StateName { get; set; }

        public EventHandler Enter { get; set;}
        public EventHandler Exit { get; set; }

        public State(string name) {
            this.StateName = name;
        }

        public State(int num) {
            this.StateName = num.ToString();
        }

        public override int GetHashCode() {
            return this.StateName.GetHashCode();
        }

        public override bool Equals(object obj) {
            StateTransition other = obj as StateTransition;
            return other != null
                && this.StateName.Equals(other);
        }

        public override string ToString() {
            return StateName;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FirmwareTester.BLL;

namespace FirmwareTester.Misc {
    public class StateMachine {
        public State CurrentState { get; private set; }
        public Dictionary<StateTransition, State> TransitionTable { get; private set; }

        public StateMachine() {
            this.TransitionTable = new Dictionary<StateTransition, State>();
        }

        public StateMachine(State initialState) : this() {
            this.CurrentState = initialState;
        }

        public void AddTransition(State fromState, State toState, string transitionName) {
            TransitionTable.Add(new StateTransition(fromState, transitionName), toState);
        }

        public State GetNext(string transitionName) {
            StateTransition transition = new StateTransition(CurrentState, transitionName);
            State nextState;
            if (!TransitionTable.TryGetValue(transition, out nextState)) {
                throw new Exception("Invalid transition: " 
                    + this.CurrentState + " -> " + transitionName);
            }
            return nextState;
        }

        public State MoveNext(string transitionName) {
            State nextState = GetNext(transitionName);
            if (this.CurrentState.Exit != null) {
                this.CurrentState.Exit(this, EventArgs.Empty);
            }
            if (nextState.Enter != null) {
                nextState.Enter(this, EventArgs.Empty);
            }
            this.CurrentState = nextState;
            return this.CurrentState;
        }

    }
}

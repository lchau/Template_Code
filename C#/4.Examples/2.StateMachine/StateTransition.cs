﻿// Defines a transition. A transition belongs
// to a state. May eventually fire events on transition.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirmwareTester.Misc {
    public class StateTransition {
        readonly string TransitionName; // Command linking this transition
        readonly State State; // State that owns this transition

        public StateTransition(State state, string transitionName) {
            this.State = state;
            this.TransitionName = transitionName;
        }

        public override int GetHashCode() {
            return 17 
                + 31 * this.State.GetHashCode() 
                + 31 * this.TransitionName.GetHashCode();
        }

        public override bool Equals(object obj) {
            StateTransition other = obj as StateTransition;
            return other != null 
                && this.State.Equals(other.State) 
                && this.TransitionName.Equals(other.TransitionName);
        }
    }
}

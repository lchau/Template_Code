﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Really basic TCP string server. Clients request
 * access to server and the server will echo string
 * responses. Will maybe update this to be capable
 * of passing serializable data or objects.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using TcpClientServer;

namespace TcpServer {
    class Program {
        private static IPAddress ipAddr;
        private static int port;

        static void Main(string[] args) {
            Console.WriteLine("Tcp Server by Larry Chau");
            if (Program.ParseInputs(args)) {
                TcpServerService server = new TcpServerService(ipAddr, port);
                server.Start();

                Console.WriteLine("Press any key to terminate...");
                Console.WriteLine("Waiting for a connection...");

                while (!Console.KeyAvailable) {
                }
            } else {
                Console.WriteLine("Invalid inputs!");
            }

            Console.ReadKey();
            Environment.Exit(0);
        }

        static bool ParseInputs(string[] args) {
            string address, portStr;
            bool validInput = true;

            // Collection
            if (args.Length > 0) address = args[0].ToUpper();
            else {
                Console.Write("Enter Server IP Address:");
                address = Console.ReadLine();
            }

            if (args.Length > 1) portStr = args[1].ToUpper();
            else {
                Console.Write("Enter Server Port:");
                portStr = Console.ReadLine();
            }

            // Validation
            try { Program.ipAddr = IPAddress.Parse(address); } catch { validInput = false; }
            if (!int.TryParse(portStr, out port)) validInput = false;

            return validInput;
        }
    }
}

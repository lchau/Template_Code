﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace TcpClientServer {
    public class TcpClientService {
        public delegate void ReadCallback(string data);

        private Task clientLoop;
        private volatile bool killClient;
        private TcpConnection connection;
        private ReadCallback readCallback;

        public TcpClientService(IPAddress ipAddr, int port) {
            this.connection = new TcpConnection(ipAddr, port);
            this.clientLoop = new Task(ClientLoop);
            this.clientLoop.Start();
            this.readCallback = null;
        }

        public void Start() {
            this.killClient = false;
            this.clientLoop.Start();
        }

        public void Stop() {
            this.killClient = true;
            while (!this.clientLoop.IsCompleted) { }
            this.connection.Close();
        }

        public void Send(string data) {
            this.connection.Write(data);
        }

        public void RegisterReadCallback(ReadCallback function) {
            this.readCallback = function;
        }

        private void ClientLoop() {
            while (!killClient) {
                if (this.readCallback != null) {
                    string data = connection.Read();
                    if (data != null) this.readCallback(data);
                }
            }
        }
    }
}

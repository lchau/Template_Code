﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * The server holds a single listener and is
 * responsible for many clients. It proxys the
 * TcpListener class to an extent. Accepts strings.
 */

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpClientServer {
    public class TcpServerService {
        public volatile int MsRefreshRate = 100;
        public int AcceptedClientCount {
            get {
                return this.clients.Count;
            }
            private set {
            }
        }

        private TcpListener server;
        private List<TcpConnection> clients;

        private Task serverLoop;
        private bool killServer;

        // Takes a visible address and port to communicate
        public TcpServerService(IPAddress address, int port) {
            this.server = new TcpListener(address, port);
            this.serverLoop = new Task(ServerLoop);
            this.clients = new List<TcpConnection>();
            this.killServer = false;
        }

        public void Start() {
            this.killServer = false;
            this.server.Start();
            this.serverLoop.Start();
        }

        public void Stop() {
            this.killServer = true;
            while (!this.serverLoop.IsCompleted) { }
            this.server.Stop();
        }

        private void ServerLoop() {
            int startTime;
            int elapsedTime;

            do {
                startTime = Environment.TickCount;

                this.AcceptPendingClients();
                this.DropDisconnectedClients();

                foreach (TcpConnection conn in clients) {
                    string data = conn.Read();
                    // echo it back
                    if (data != null) {
                        Console.WriteLine("Received: {0}", data);
                        conn.Write(data.ToUpper());
                        Console.WriteLine("Sent: {0}", data.ToUpper());
                    }
                }

                // Enforce Refresh Rate
                elapsedTime = Environment.TickCount - startTime;
                if (elapsedTime < MsRefreshRate) Thread.Sleep(MsRefreshRate - elapsedTime);
            } while (!killServer);

            foreach (TcpConnection conn in clients) {
                conn.Close();
            }
        }

        private void AcceptPendingClients() {
            if (this.server.Pending()) {
                TcpConnection conn
                    = new TcpConnection(this.server.AcceptTcpClient(), 256);
                this.clients.Add(conn);
                if (conn.Connected)
                    Console.WriteLine("Connected!");
            }
        }

        private void DropDisconnectedClients() {
            TcpConnection conn;
            for (int i = clients.Count-1; i >= 0; i--) {
                conn = clients[i];
                if (!conn.Connected) {
                    conn.Close();
                    clients.Remove(conn);
                    Console.Write("Dropped: " + "IP - " + conn.RemoteAddress 
                        + ":" + conn.RemotePort.ToString());
                }
            }
        }
    }
}

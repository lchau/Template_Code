﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Proxies the TcpClient with stream handling logic.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TcpClientServer {
    public class TcpConnection {
        // Packet size
        public int ReadBufferSize { get; private set; }

        private TcpClient tcpClient;

        public bool Connected {
            get {
                try {
                    Socket socket = tcpClient.Client;
                    return !(socket.Poll(1, SelectMode.SelectRead)
                        && socket.Available == 0);
                } catch (SocketException) { return false; }
            }
            private set { }
        }
        public string LocalAddress { get; private set; }
        public int LocalPort { get; private set; }
        public string RemoteAddress { get; private set; }
        public int RemotePort { get; private set; }

        private TcpConnection() { }

        public TcpConnection(IPAddress ipAddr, int port) 
            : this(new TcpClient(ipAddr.ToString(), port), port) { }

        public TcpConnection(TcpClient client, int readBufferSize) {
            if(client == null)
                throw new InvalidOperationException("Client is null.");

            this.tcpClient = client;
            this.ReadBufferSize = readBufferSize;
            this.LocalAddress = ((IPEndPoint)tcpClient.Client.LocalEndPoint).Address.ToString();
            this.LocalPort = ((IPEndPoint)tcpClient.Client.LocalEndPoint).Port;
            this.RemoteAddress = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
            this.RemotePort = ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Port;
        }

        public void Close() {
            this.tcpClient.GetStream().Close();
            this.tcpClient.Close();
        }

        // Receives all available bytes on each call.
        public string Read() {
            string data = null;
            NetworkStream stream = this.tcpClient.GetStream();

            if (stream.CanRead) {
                Byte[] buffer = new Byte[this.ReadBufferSize];
                int i;
                while (stream.DataAvailable) {
                    i = stream.Read(buffer, 0, buffer.Length);

                    // Translate data bytes to a ASCII string.
                    string rxData = Encoding.ASCII.GetString(buffer, 0, i);

                    // Process the data sent by the client.
                    data = string.Concat(data, rxData);
                }
            }

            return data;
        }

        public void Write(string txData) {
            if (txData == null || txData == "")
                throw new ArgumentException("Cannot write null/empty string");

            NetworkStream stream = this.tcpClient.GetStream();

            byte[] msg = System.Text.Encoding.ASCII.GetBytes(txData);

            stream.Write(msg, 0, msg.Length);
        }
    }
}

﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Really basic TCP string client.
 * to pass back and forth messages. Will
 * maybe update this to be capable of passing
 * serializable data or objects.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Sockets;
using TcpClientServer;

namespace TcpClient {
    class Program {
        private static IPAddress ipAddr;
        private static int port;
        private static TcpClientService clientService;

        static void Main(string[] args) {
            Console.WriteLine("Tcp Client by Larry Chau");
            if (Program.ParseInputs(args) && TryConnect(ipAddr, port)) {
                Console.WriteLine("Type \"exit\" to terminate...");
                Console.WriteLine("Otherwise, type string to send...");

                string input = Console.ReadLine();

                while (input != "exit") {
                    Program.TrySend(input);
                    Console.WriteLine("Sent: {0}", input);
                    input = Console.ReadLine();
                }

                Console.WriteLine("Closing connection and exiting...");
                clientService.Stop();
            } else {
                Console.WriteLine("Invalid inputs!");
            }

            Console.ReadKey();
            Environment.Exit(0);
        }

        static bool TryConnect(IPAddress ipAddr, int port) {
            try {
                clientService = new TcpClientService(ipAddr, port);
                clientService.RegisterReadCallback(Program.ReadCallback);
                return true;
            } catch (SocketException e) {
                Console.WriteLine("Error: " + e.Message);
                return false;
            }
        }

        static void TrySend(string data) {
            try {
                clientService.Send(data);
            } catch (ArgumentException e) {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        static void ReadCallback(string data) {
            Console.WriteLine("Received: {0}", data);
        }

        static bool ParseInputs(string[] args) {
            string address, portStr;
            bool validInput = true;

            // Collection
            if (args.Length > 0) address = args[0].ToUpper();
            else {
                Console.Write("Enter Server IP Address:");
                address = Console.ReadLine();
            }

            if (args.Length > 1) portStr = args[1].ToUpper();
            else {
                Console.Write("Enter Server Port:");
                portStr = Console.ReadLine();
            }

            // Validation
            try { Program.ipAddr = IPAddress.Parse(address); } catch { validInput = false; }
            if (!int.TryParse(portStr, out port)) validInput = false;

            return validInput;
        }
    }
}

﻿// Simple console wrapper performing double buffering.
// https://en.wikipedia.org/wiki/Multiple_buffering for more details.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirmwareTester.Presentation {
    public class KeyOrLine {
        public ConsoleKeyInfo KeyInfo;
        public string Line;
        public KeyOrLine(ConsoleKeyInfo key, string line) {
            this.KeyInfo = key;
            this.Line = line;
        }
    }
    public class DoubleBuffer {
        public int RefreshRate;
        private int lastBufferSwapTick;

        private StringBuilder[] displayBuffer;

        private KeyOrLine keyOrLine;
        private string inputBuffer { get; set; }
        public bool InputEN { get; private set; }
        private bool nextInputEN;

        private int bufferIdx;

        public DoubleBuffer() {
            this.displayBuffer = new StringBuilder[2];
            this.displayBuffer[0] = new StringBuilder();
            this.displayBuffer[1] = new StringBuilder();
            this.inputBuffer = "";
            this.bufferIdx = 0;
            this.RefreshRate = 50;
            this.lastBufferSwapTick = Environment.TickCount;
            this.InputEN = false;
            this.nextInputEN = true;
        }

        public KeyOrLine GetInput() {
            KeyOrLine keyLine = this.keyOrLine;
            this.keyOrLine = null;
            return keyLine;
        }

        public void SetInputEN(bool tf) {
            this.nextInputEN = tf;
        }

        public void Write(string text) {
            this.displayBuffer[bufferIdx].Append(text);
        }

        public void WriteLine(string line = "") {
            this.Write(line + Environment.NewLine);
        }

        public void Write(char key) {
            this.displayBuffer[bufferIdx].Append(key);
        }

        public void SwapBuffers() {
            this.InputEN = nextInputEN;
            if (!displayBuffer[0].ToString().Equals(displayBuffer[1].ToString())) {
                Console.Clear();
                Console.Write(displayBuffer[bufferIdx]);
                displayBuffer[bufferIdx].Clear();
                bufferIdx = 1 - bufferIdx;
                Console.Write(this.inputBuffer);
            }
            this.keyOrLine = this.ParseKeyOrLine();
            int elapsed = Environment.TickCount - this.lastBufferSwapTick;
            if (elapsed < RefreshRate) {
                System.Threading.Thread.Sleep(RefreshRate - elapsed);
            }
            this.lastBufferSwapTick = Environment.TickCount;
        }

        // Parses console key or string
        private KeyOrLine ParseKeyOrLine() {
            ConsoleKeyInfo key;
            while (Console.KeyAvailable) {
                key = Console.ReadKey(true);
                if (!this.InputEN) continue; // throw it away
                // alphanumeric
                if ((int)key.KeyChar > 31 && (int)key.KeyChar < 127) {
                    inputBuffer += key.KeyChar;
                    Console.Write(key.KeyChar);
                } else if (key.KeyChar == '\b' && inputBuffer.Length > 0) {
                    inputBuffer = inputBuffer.Remove(inputBuffer.Length - 1);
                    Console.Write("\b \b");
                } else if (key.KeyChar == '\r') {
                    KeyOrLine keyOrLine = new KeyOrLine(key, inputBuffer);
                    this.inputBuffer = "";
                    Console.Write(key.KeyChar);
                    return keyOrLine;
                } else return new KeyOrLine(key, null);
            }
            return null;

        }

    }
}

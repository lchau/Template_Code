﻿/* Larry Chau
 * Last Modified: 10/13/2017
 * Command Line User Interface Class
 * ---------------------------------
 * Middling layer between buffer and view. This class is specific
 * to defining a command line interface that is structured with menus
 * UI is structured as a loop presenting menu commands with a single
 * line feedback for display. Only input of text or numbers to control
 * stepping through these menu items. Pattern could be called a chain
 * of responsibility.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using FirmwareTester.Misc;

namespace FirmwareTester.Presentation.CmdLine {
    // A command line can be displaying an appropriately formatted menu, or running a defined routine
    // where the display is blank and up to the user.
    public class CmdLineUI : ActiveObject {
        public int CmdTimeoutMs = 15000; // timeout associated with UI commands
        public int SelectedNum; // SelectedNum = -1 when nothing is selected

        public string Header;
        public string Display;
        public string Error;

        private CmdLineDisplayable currDisplayable;
        private DoubleBuffer doubleBuffer;

        private struct IdRequest {
            public int Min, Max;
            string Prefix;
        }

        public CmdLineUI(CmdLineDisplayable startDisplayable) {
            this.SelectedNum = -1;
            this.Header = "";
            this.Display = "";
            this.Error = "";
            this.currDisplayable = startDisplayable;
            this.currDisplayable.ParentMenu = null;
            this.doubleBuffer = new DoubleBuffer();
            this.Execute = this.Process; // assign loop for active object
        }

        public void SetInputEN(bool tf) {
            this.doubleBuffer.SetInputEN(tf);
        }

        public int ReadId(int min = 1, int max = int.MaxValue, string prefix = "->") {
            if (min < 1) throw new ArgumentOutOfRangeException("Id less than 1 does not make sense");
            // TODO: Finish this...
            return 0;
        }

        private object Process(string instruction, EventHandler callBack, params object[] parameters) {
            if (instruction == "kill") 
                return null;
            try {
                this.ShowDisplayable();
                this.currDisplayable = this.GetNextDisplayable();
            } catch (DisplayableException d) { // TODO: Might want to remove this now...
                this.Display = d.Message;
            } catch (Exception e) {
                this.Error = e.Message;
                this.SendInstruction("kill");
                this.Exit();
            } finally {
                this.doubleBuffer.SwapBuffers();
            }
            return null;
        }

        #region helper routines

        private void ShowDisplayable() {
            if (this.currDisplayable is CmdLineMenu)
                this.PrintMenu();
            else if (this.currDisplayable is CmdLineRoutine)
                ((CmdLineRoutine)this.currDisplayable).Routine();
        }

        private void PrintMenu() {
            if (!string.IsNullOrEmpty(this.Header)) {
                this.doubleBuffer.WriteLine(this.Header);
                this.doubleBuffer.WriteLine();
            }

            if (!string.IsNullOrEmpty(this.Display)) {
                this.doubleBuffer.WriteLine(this.Display);
                this.doubleBuffer.WriteLine();
            }
            this.doubleBuffer.WriteLine(this.currDisplayable.ToString());
        }

        #region Menu Structure

        #endregion Menu Structure

        private CmdLineDisplayable GetNextDisplayable() {
            CmdLineDisplayable nextDisp;
            if (this.currDisplayable is CmdLineMenu)
                nextDisp = this.RequestOption();
            else if (this.currDisplayable is CmdLineRoutine) {
                nextDisp = ((CmdLineRoutine)this.currDisplayable).Next;
                nextDisp.ParentMenu = this.currDisplayable.ParentMenu;
            } else
                throw new InvalidOperationException("Could not determine current displayable.");
            
            // Just run the routine if it is part of the current menu
            // recursive call to get next "Displayable" menu/routine.
            CmdLineRoutine nextRoutine = nextDisp as CmdLineRoutine;
            if (nextRoutine != null && nextRoutine.EnableMenu) {
                nextRoutine.Routine();
                return this.GetNextDisplayable();
            }

            return nextDisp;
        }

        private CmdLineDisplayable RequestOption() {
            CmdLineDisplayable nextDisp;
            CmdLineMenu currMenu = (CmdLineMenu) this.currDisplayable;
            int optionNum = this.ReadId(1, currMenu.OptionsCount);
            if (optionNum > 0) { // option is valid
                nextDisp = currMenu.GetOption(optionNum);
                nextDisp.ParentMenu = currMenu;
                return nextDisp;
            }
            return currMenu;
        }

        private void Exit() {
            int startTime = Environment.TickCount;
            int elapsed;
            do {
                doubleBuffer.SwapBuffers();
                if (!string.IsNullOrEmpty(this.Error)) {
                    this.doubleBuffer.WriteLine();
                    this.doubleBuffer.WriteLine("ERROR: " + this.Error);
                }
                this.doubleBuffer.WriteLine();
                this.doubleBuffer.Write("Terminating in...");
                elapsed = Environment.TickCount - startTime;
                this.doubleBuffer.Write((5 - elapsed / 1000).ToString() + "s");
                Thread.Sleep(50);
            } while (!Console.KeyAvailable && elapsed < 5000);
        }

        // Return 0 if no input, return -1 if invalid
        private int RequestId(int min = 1, int max = int.MaxValue, string prefix = "->") {
            if (this.doubleBuffer.InputEN) this.doubleBuffer.Write(prefix);
            else this.doubleBuffer.Write("X>");

            int num = -1;
            KeyOrLine keyOrLine = this.doubleBuffer.GetInput();
            if (keyOrLine == null || keyOrLine.Line == null) return 0;
            if (!int.TryParse(keyOrLine.Line, out num) || num < min || num > max) {
                this.Display = "Invalid Argument. Try again.";
                //this.requestLevel = 0;
                return -1;
            }
            return num;
        }

        #endregion helper routines

    }
}

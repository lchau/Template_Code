﻿/* Larry Chau
 * Last Modified: 07/26/2017
 * Command Line View
 * ---------------------------------
 * Command line control. Generally to help consolidate and prototype functionality
 * that will be the GUI and to shape the front controller (program class), with
 * the understanding that refactoring a command line is a less time-consuming practice.
 * Should remove technical debt propgating from the application layer to the GUI.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using FirmwareTester.App.ViewModel;
using FirmwareTester.App;
using FirmwareTester.Misc;

namespace FirmwareTester.Presentation.CmdLine {
    public class CmdLineView {
        // Data specific to this view. The ViewModel.
        private Program program;
        private CmdLineUI cmdLineUI;
        private Guid instrID;
        private object[] programRetVals;

        private CmdLineView menu;
        private ChannelData[] channels;

        private bool loading;

        private int progCmdStartTime;
        private long[] deviceIds;

        private long selectedDeviceId;

        public CmdLineView(Program program) {
            this.program = program;
            this.loading = false;
            this.progCmdStartTime = -1;
            this.selectedDeviceId = -1;
            this.program.OnReturnValue += new EventHandler<CallbackArgs>(this.OnReturnValue);
            Console.CancelKeyPress += new ConsoleCancelEventHandler(program.Terminate);
            this.deviceIds = new long[Program.MAX_DEVICES];

            this.BuildUI();
        }

        public void Run() {
            this.cmdLineUI.Launch();
            new Thread(new ThreadStart(this.UpdateHeader)).Start(); // method to update header
            while (this.cmdLineUI.IsAlive())
                Thread.Sleep(100);
        }

        ~CmdLineView() {
            this.program.OnReturnValue -= new EventHandler<CallbackArgs>(this.OnReturnValue);
        }

        #region menus

        // Assumes we are connected to the device
        /* private void ManualControl() {
            Device device = this.program.GetDevice(this.selectedDeviceId);
            switch (device.DeviceType) {
                case (DeviceType.ControlUnit):
                    this.ControlUnitManual(device);
                    break;
                case (DeviceType.Controller):
                    break;
            }
        }
        */

        #endregion

        #region routines and callbacks

        private void FindDevice() {
            this.cmdLineUI.Display = "Finding devices...";
            this.cmdLineUI.SetInputEN(false);
            this.program.FindDevices(OnFindDevice);
        }

        private void OnFindDevice(object sender, EventArgs e) {
            if (this.program.GetDeviceIds().Length > 0) this.cmdLineUI.Display += "Found!";
            else this.cmdLineUI.Display += "No devices found.";
            this.cmdLineUI.SetInputEN(true);
        }

        private void ConnectDevice() {
            long[] deviceIds = this.program.GetDeviceIds();
            long idIdx = (int)this.cmdLineUI.ReadId(1, deviceIds.Length);
            this.selectedDeviceId = deviceIds[idIdx];
            this.cmdLineUI.Display = "Connecting to device...";
            this.cmdLineUI.SetInputEN(false);
            this.program.ConnectDevice(this.selectedDeviceId, OnBoolCallback);
        }

        private void DisconnectDevice() {
            this.cmdLineUI.Display = "Disconnecting device...";
            this.cmdLineUI.SetInputEN(false);
            this.program.DisconnectDevice(this.selectedDeviceId, OnBoolCallback);
        }

        private void OnBoolCallback(object sender, EventArgs e) {
            CallbackArgs callbackArgs = (CallbackArgs)e;
            if ((bool)callbackArgs.ReturnValue)
                this.cmdLineUI.Display += "...success!";
            else
                this.cmdLineUI.Display += "...failed!";
            this.cmdLineUI.SetInputEN(true);
        }

        /* private void ControlUnitManual(Device device) {
            this.doubleBuffer.WriteLine("Refresh Rate: "
                + this.doubleBuffer.RefreshRate.ToString() + "ms");
            this.PrintChannels(this.selectedDeviceId);
            this.doubleBuffer.WriteLine();

            this.PrintDisplay();

            this.doubleBuffer.WriteLine("1. Change Direction");
            this.doubleBuffer.WriteLine("2. Toggle Digital");
            this.doubleBuffer.WriteLine("3. Set Analog voltage");
            this.doubleBuffer.WriteLine("4. Return");

            if (this.requestLevel == 0) this.selectedNum = RequestInt(1, 4);

            switch (this.selectedNum) {
                case 1:
                    this.ChangeDirection();
                    break;
                case 2:
                    this.ToggleDigital();
                    break;
                case 3:
                    this.SetVoltage();
                    break;
                case 4:
                    this.ChangeMenu(CmdLineMenu.MAIN);
                    break;
            }
        }
        */

        /* private void PrintChannels(long id) {
            this.channels = this.program.GetChannelData(id);
            this.doubleBuffer.WriteLine("\tName\tI/O\tState/Voltage");
            int i = 1;
            foreach (ChannelData datum in this.channels)
                this.doubleBuffer.WriteLine(i++.ToString() + ".\t" 
                    + datum.ToString().Replace(';', '\t'));
        }
        */

        /* private void SendSerialCmds() {
            // TODO: Create a trace file
            // TODO: get timestamp of command
            // TODO: Make SendSerialCmd an API Method, move sendinstruction into protected
            this.doubleBuffer.Write("Controller Number: ");
            int controllerNum = this.RequestInt(1, this.program.GetCtrlrCount());
            if (controllerNum < 0) return;
            if (!this.program.IsCtrlrConnected(controllerNum-1)) {
                this.display = "Controller " + controllerNum + " is not connected.";
                return;
            }
            // Console.Clear()
            this.doubleBuffer.WriteLine("-------------[ESC] to return-----------");
            this.doubleBuffer.WriteLine("-------[DEL] to show full output-------");
            this.doubleBuffer.WriteLine();
            KeyOrLine keyOrLine;
            string input = "";
            ConsoleKeyInfo keyInfo;
            bool raw = false;
            /*
            do {
                keyOrLine = this.doubleBuffer.ParseKeyOrLine();
                keyInfo = keyOrLine.KeyInfo;
                input = keyOrLine.Line;
                if (keyOrLine.KeyInfo.Key == ConsoleKey.Delete) {
                    raw = !raw;
                    this.doubleBuffer.WriteLine("Raw Response: " + raw.ToString());
                    this.doubleBuffer.WriteLine();
                    continue;
                }
                if (input == null) continue;
                this.instrID = this.program.SendInstruction("send serial command", controllerNum - 1, input, raw);
                if (this.program.WaitForFinish(this.instrID, this.CmdTimeoutMs)) {
                    string response = (string)this.programRetVals[0];
                    if (raw) response = Regex.Replace(Regex.Escape(response),"\0","\\0");
                    this.doubleBuffer.Write("->" + response + "\n");
                    this.doubleBuffer.WriteLine();
                } else break;
                this.programRetVals = null;
            } while (keyInfo.Key != ConsoleKey.Escape);
            this.programRetVals = null;
            this.display = "";
            */
        //}

        private void ToggleDigital() {
            /*
            this.doubleBuffer.Write("Channel:");
            int num = RequestInt(1, this.digiChannels.Length);
            if (num < 0 || !this.CheckATEConnected(this.selectedATE)) return;
            try {
                this.program.WriteDigital(this.selectedATE, num - 1,
                    !this.digiChannels[num - 1].State);
            } catch (InvalidOperationException e) {
                this.display = e.Message;
            }
            */
        }

        /* private void SetVoltage() {
            this.doubleBuffer.Write("Channel:");
            //int num = RequestInt(this.digiChannels.Length+1, this.);
            //if (num < 0 || !this.CheckATEConnected(this.selectedATE)) return;
        }
        */

        private void BuildUI() {
            // Routines
            CmdLineRoutine findDevices = new CmdLineRoutine("Find Devices", this.FindDevice, true);
            CmdLineRoutine connectDevice = new CmdLineRoutine("Connect Device", this.ConnectDevice, true);
            CmdLineRoutine disconnectDevice = new CmdLineRoutine("Disconnect Device", this.DisconnectDevice, true);
            //CmdLineRoutine manualControl;

            // Menus
            CmdLineMenu connections = new CmdLineMenu("Connections", new CmdLineDisplayable[3]{findDevices, connectDevice, disconnectDevice});
            //CmdLineMenu ateControl = new CmdLineMenu("ATEControl",new string[3]{"Change Direction","Toggle Digital","Set Analog Voltage"});
            CmdLineMenu main = new CmdLineMenu("Main", new CmdLineDisplayable[1]{connections});

            // Mappings
            findDevices.Next = connections;

            this.cmdLineUI = new CmdLineUI(main);
        }

        // Device information underneath header...
        private void UpdateHeader() {
            while (this.cmdLineUI.IsAlive()) {
                this.cmdLineUI.Header = "Firmware Tester " + "v" + Program.Version.ToString("0.000") 
                    + " by Larry Chau" + Environment.NewLine;
                this.cmdLineUI.Header += this.GetDevicesData();
                Thread.Sleep(100);
            }
        }

        /* private void OnGetAnalogChannels(object sender, EventArgs e) {
            CallbackArgs args = e as CallbackArgs;
            if (e != null) {
                this.analogChannels = (AnalogChannel[])args.ReturnValue;
            }
        }
        */

        #endregion

        #region GUI helpers

        // TODO: Add a delay to this...?
        /*
        private void PrintLoading() {
            if (this.loading) this.display += '.';
        }
        */

        private string GetDevicesData() {
            int i;
            string retVal = "";
            this.deviceIds = this.program.GetDeviceIds();
            if (this.deviceIds.Length > 0) {
                retVal = "--------Devices--------" + Environment.NewLine;
                i = 0;
                foreach (long id in this.deviceIds) {
                    Device device = this.program.GetDevice(id);
                    retVal += (++i).ToString() + "." + device.ToString();
                }
                retVal += Environment.NewLine;
            }
            return retVal;
        }

        #endregion

        #region functional helpers

        /*
        private bool CheckATEConnected(int ateIdx) {
            if (!this.program.IsCtrlUnitConnected(ateIdx)) {
                this.display = "ATE " + (ateIdx + 1).ToString() + " is no longer connected.";
                return false;
            }
            return true;
        }

        private void ChangeDirection() {
            this.requestLevel = 1;
            int channelNum = this.RequestInt(1, this.channels.Length, "Channel Number:");
            if (channelNum < 1) return;
            this.requestLevel = 0;
            this.display = "";
            this.channels[channelNum - 1].Direction = !this.channels[channelNum - 1].Direction;
            return;
        }

        private void ChangeMenu(CmdLineMenu submenu) {
            this.display = "";
            this.menu = submenu;
            // Console.Clear();
        }
        */

        /*
        // TODO: Delete as part of removing sendinstr? 8-3-2017
        // Used for real time menu
        private bool CheckForResponse(Guid uniqueID) {
            if (uniqueID.Equals(Guid.Empty)) return true;
            if(this.progCmdStartTime < 0) this.progCmdStartTime = Environment.TickCount;
            int elapsed = Environment.TickCount - this.progCmdStartTime;
            bool isFinished = this.program.IsFinished(uniqueID);

            if (!isFinished && elapsed < CmdTimeoutMs) {
                this.display += '.';
            } else if (elapsed >= CmdTimeoutMs) {
                this.state = CmdLineState.EXIT;
                this.error = "Timed out waiting for program to respond.";
            } else {
                this.instrID = Guid.Empty;
                this.doubleBuffer.SetInputEN(true);
                this.progCmdStartTime = -1;
            }

            return isFinished;
        }
        */

        private void OnReturnValue(object sender, CallbackArgs e) {
            this.programRetVals = e.Parameters;
        }

        #endregion
    }
}

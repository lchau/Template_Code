﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// TODO: Add timeout on routine calls?

namespace FirmwareTester.Presentation.CmdLine {
    public class CmdLineRoutine : CmdLineDisplayable {
        public bool EnableMenu; // On blank page or change display of existing menu, (Graph Skip)
        public CmdLineDisplayable Next { get; set; } // Goes to either another menu or routine, or until keypress?
        public Action Routine { get; private set; }

        public CmdLineRoutine(string name, Action routine, bool enableMenu = true) {
            this.Name = name;
            this.Routine = routine;
            this.EnableMenu = enableMenu;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirmwareTester.Presentation.CmdLine {
    public class CmdLineDisplayable {
        public string Name;
        public CmdLineMenu ParentMenu;

        protected CmdLineDisplayable() { }

        public CmdLineDisplayable(string name) {
            this.Name = name;
            this.ParentMenu = null;
        }
    }
}

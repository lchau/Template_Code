﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirmwareTester.Presentation.CmdLine {
    public class CmdLineMenu : CmdLineDisplayable {
        public bool EnableReturn = true; // Allow us to go back a menu
        public string[] Options {
            get {
                return subOptions.Select(d => d.Name).ToArray();
            }
            private set { }
        }
        public int OptionsCount {
            get {
                if (this.EnableReturn) return this.subOptions.Length + 1;
                else return this.subOptions.Length;
            }
            private set { }
        }
        private CmdLineDisplayable[] subOptions; // Menu options

        public CmdLineMenu(string name, CmdLineDisplayable[] subOptions) : base(name) {
            this.subOptions = subOptions;
        }

        public CmdLineDisplayable GetOption(int optionNum) {
            if (this.EnableReturn && optionNum == this.OptionsCount)
                return this.ParentMenu;
            else return this.subOptions[optionNum-1];
        }

        public override string ToString() {
            string options = "";
            int menuNum = 1;

            foreach (CmdLineDisplayable menu in subOptions)
                options += (menuNum++).ToString() + "." + menu.Name + Environment.NewLine;

            if (this.EnableReturn) {
                if (this.ParentMenu != null) options += menuNum.ToString() + "." + "Return" + Environment.NewLine;
                else options += menuNum.ToString() + "." + "Exit" + Environment.NewLine;
            }

            return options;
        }
    }
}

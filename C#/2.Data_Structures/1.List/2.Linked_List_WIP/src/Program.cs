﻿/* Author: Larry Chau
 * Hello World for C#, a classic.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hello_World { /* Otherwise would be in global namespace */
    class Program { /* Function must belong to a class */
        static void Main(string[] args) {
            Console.WriteLine("Hello, World!");
            Console.ReadKey();
        }
    }
}

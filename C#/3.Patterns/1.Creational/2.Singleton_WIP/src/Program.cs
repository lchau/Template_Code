﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Proxy pattern example.
 * A driver drives cars only if the driver is over the age of 16.
 * The ProxyCar class determines if the driver is of appropriate
 * age to drive. 
 * 
 * It functions as an intermediary, or proxy, between
 * the driver and the car class. It is a wrapper that is being 
 * called by the client class to access a real serving object
 * behind the scenes.
 * 
 * Relationship between proxy and the real object is set at compile
 * time whereas a decorator is assigned to the object at runtime.
 * 
 * Reasons to use:
 *  Access to an object should be controlled (typically because it is
 *      resource intensive)
 *  Additional functionality should be provided when accessing an object.
 *      (Single responsibility principle)
 * 
 * https://en.wikipedia.org/wiki/Proxy_pattern
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proxy {
    class Program {
        static void Main(string[] args) {
            Console.WriteLine(
                "Attempting to drive a proxyCar with an age 15 driver...");
            ICar car = new ProxyCar(new Driver(15));
            car.DriveCar();

            Console.WriteLine( Environment.NewLine +
                "Attempting to drive a proxyCar with an age 25 driver..");
            car = new ProxyCar(new Driver(25));
            car.DriveCar();

            Console.ReadKey();
        }
    }
}

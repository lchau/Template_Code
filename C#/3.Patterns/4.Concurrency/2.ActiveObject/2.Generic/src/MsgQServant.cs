﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * The real object. Nothing changed here.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class MsgQServant {
        public static int DEFAULT_LIMIT = 200;

        private Queue<string> msgQ { get; set; }
        public int Limit { get; private set; }

        public MsgQServant(int size) {
            this.msgQ = new Queue<string>();
            this.Limit = DEFAULT_LIMIT;
        }

        public void Put(string message) {
            this.msgQ.Enqueue(message);
        }

        public string Get() {
            return this.msgQ.Dequeue();
        }

        public bool isEmpty() {
            return this.msgQ.Count == 0;
        }

        public bool isFull() {
            return this.msgQ.Count >= this.Limit;
        }
    }
}

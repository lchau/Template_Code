﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Made into a generic scheduler. This is an assumption
 * on my part that all active objects will execute requests
 * on first-available-first-serve basis.
 * 
 * This of course can be made less generic by making an IScheduler
 * and creating different schemes (e.g. round-robin).
 */
 
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;

namespace ActiveObject {
    class AOScheduler {

        // Maintains a bounded buffer of pending method
        // requests created by the proxy. This queue keeps
        // track of which method requests to execute. Also
        // decouples the client thread from the servant
        // thread so the two threads can run concurrently.
        private BlockingCollection<MethodRequest> actQueue;
        private Thread thread;
        private volatile bool exitThread;

        // Initialize the Activation_Queue to have the
        // specified capacity and make the Scheduler
        // run in its own thread of control.
        public AOScheduler(int high_water_mark) {
            this.actQueue
                = new BlockingCollection<MethodRequest>(new ConcurrentBag<MethodRequest>(), high_water_mark);

            // Spawn a separate thread to dispatch method requests.
            this.thread = new Thread(this.Dispatch);
            this.exitThread = false;
            this.thread.Start();
        }

        ~AOScheduler() {
            this.thread.Abort();
        }

        // Signal thread to stop at a safe spot.
        public void Stop() {
            this.exitThread = true;
        }

        // Insert the Method_Request into the
        // Activation_Queue. This method runs
        // in the thread of its client, i.e.,
        // in the Proxy's thread.
        public void Enqueue(MethodRequest methodRequest) {
            // We always want to keep 1 item in the queue just in case
            // we are inspecting one and need to re-add so we don't end up
            // blocking forever in the thread.
            if (actQueue.Count >= actQueue.BoundedCapacity - 1)
                throw new ConstraintException("Activation Queue is full");
            if (!actQueue.TryAdd(methodRequest))
                throw new InvalidOperationException("Could not add method request to activation queue");
        }

        // Dispatch the Method_Request's on their Servant
        // in the Scheduler's thread.
        public void Dispatch() {
            // Iterate continuously in a separate thread
            while (true) {
                for (int i = actQueue.Count - 1; i >= 0; i--) {
                    MethodRequest methodRequest;
                    bool removed = actQueue.TryTake(out methodRequest);

                    // Select a method request whose guard evaluates to true
                    if (methodRequest.Guard() && removed) methodRequest.Call();
                    else actQueue.Add(methodRequest); // Re-add if guard isn't working.
                }
            }
        }

    }
}

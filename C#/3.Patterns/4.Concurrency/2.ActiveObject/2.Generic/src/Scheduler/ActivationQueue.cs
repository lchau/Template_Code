﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Maintains a bounded buffer of pending method
 * requests created by the proxy. This queue keeps
 * track of which method requests to execute. Also
 * decouples the client thread from the servant
 * thread so the two threads can run concurrently.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class ActivationQueue : List<MethodRequest> {
        // Synchonization mechanisms, e.g. condition
        // variables and mutexes, and the queue
        // implementation, e.g. an array or a linked list,
        // go here.
        private readonly object mutex;

        public int HighWaterMark { get; private set; }

        // Block for an "infintie" amount of time
        // waiting for Enqueue() and Dequeue() methods
        // to complete.
        public const int INFINITE = -1;

        // Constructor creates the queue with the
        // specified high water mark that determines
        // its capacity.
        public ActivationQueue(int high_water_mark) {
            this.HighWaterMark = high_water_mark;
            this.mutex = new object();
        }

        // Insert Method_Request into the queue, waiting
        // up to msec_timeout amount of time for space to
        // become available in the queue.
        public void Insert(MethodRequest methodRequest, int msec_timeout = INFINITE) {
            lock (this.mutex) {
                int startTime = Environment.TickCount;
                int elapsedTime = Environment.TickCount - startTime;
                while ((elapsedTime <= msec_timeout || msec_timeout == INFINITE)
                    && this.Count >= this.HighWaterMark) {
                    elapsedTime = Environment.TickCount - startTime;
                }
                if (this.Count < this.HighWaterMark) this.Add(methodRequest);
            }
        }

        // Remove Method_Request from the queue, waiting
        // up to msec_timeout amount of time for a
        // Method_Request to appear in the queue. If timeout
        // exceeded, then do not call the method. (Exception?)
        public void Delete(MethodRequest methodRequest, int msec_timeout = INFINITE) {
            lock (this.mutex) {
                int startTime = Environment.TickCount;
                int elapsedTime = Environment.TickCount - startTime;
                while ((elapsedTime <= msec_timeout || msec_timeout == INFINITE) 
                    && !this.Contains(methodRequest)) {
                        elapsedTime = Environment.TickCount - startTime;
                }
                if (this.Contains(methodRequest)) this.Remove(methodRequest);
            }
        }

        // Concurrent implementation of indexer
        new public MethodRequest this[int index] {
            get {
                lock (this.mutex) {
                    if (index < 0) {
                        index = this.Count - index;
                    }
                    return base[index];
                }
            }
            set {
                lock (this.mutex) {
                    if (index < 0) {
                        index = this.Count - index;
                    }
                    base[index] = value;
                }
            }
        }
    }
}

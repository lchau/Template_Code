﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * This proxy decouples the synchronization logic from the class.
 * For this example the implementation details concerning the
 * message queue reside in the message queue class. Details about
 * prioritizing and scheduling operations on the class should not
 * be baked into the class itself. (Single-Responsibility Principle)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class MsgQProxy : IDisposable {
        // Bound the message queue size.
        public static int MAX_SIZE = 100;

        private MsgQServant msgQServant;
        private AOScheduler msgQScheduler;

        public MsgQProxy () : this(MAX_SIZE) {

        }

        public MsgQProxy (int size) {
            this.msgQServant = new MsgQServant(size);
            this.msgQScheduler = new AOScheduler(size);
        }

        // If we want to get rid of thread before garbage collection.
        public void Dispose() {
            this.msgQScheduler.Stop();
        }

        // Schedule Put Command to execute on the active object
        public void Put(string message) {
            ReflectionRequest methodRequest
                = new ReflectionRequest(this.msgQServant, "Put",
                    false, new object[]{message});

            this.msgQScheduler.Enqueue(methodRequest);
        }

        // Return a Message_Future as the "future"
        // result of an asychronous "get"
        // method on the active object. (Return value)
        public string Get() {
            MessageFuture<object> result = new MessageFuture<object>();

            ReflectionRequest methodRequest
                = new ReflectionRequest(this.msgQServant, "Get", false);
            methodRequest.BindFuture(result);

            this.msgQScheduler.Enqueue(methodRequest);
            return (string)result.Wait();
        }
    }
}

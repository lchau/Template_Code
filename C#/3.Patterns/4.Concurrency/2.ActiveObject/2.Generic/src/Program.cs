﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * See the basic variant for more details.
 * A more generic implementation of the ActiveObject
 * pattern.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class Program {
        static void Main(string[] args) {
            MsgQProxy msgQ = new MsgQProxy(); // Create active object

            msgQ.Put("Hello"); // Use just like a real object.

            Console.ReadKey(); // Wait for user to proceed.

            Console.WriteLine(msgQ.Get()); // Get the value we just stored

            msgQ.Dispose();

            Console.ReadKey();

            Environment.Exit(0); // Close terminal.
        }
    }
}

﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Make a call using reflection.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ActiveObject {
    class ReflectionRequest : MethodRequest {
        private object servant;
        private object[] parameters;

        private MethodBase methodBase;
        private MessageFuture<object> msgFuture;

        public ReflectionRequest(object servant, string method,
            bool isStatic, params object[] parameters) {
                BindingFlags flags = BindingFlags.Instance;
                if (isStatic) flags = BindingFlags.Static;

                Type[] paramTypes = parameters.Select(o => o.GetType()).ToArray();

                this.methodBase = servant.GetType().GetMethod(
                    method,
                    flags | BindingFlags.Public,
                    null,
                    paramTypes,
                    null
                    );

                this.servant = servant;
                this.parameters = parameters;
        }

        public void BindFuture(MessageFuture<object> msgFuture) {
            this.msgFuture = msgFuture;
        }

        // Process immediately unless we override.
        public virtual bool Guard() {
            return true;
        }

        public void Call() {
            object retVal = this.methodBase.Invoke(this.servant, this.parameters);
            if (this.msgFuture != null) this.msgFuture.SetFuture(retVal);
        }
    }
}

﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Made return value a generic object. Can be
 * specified using passed in type.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class MessageFuture<T> {
        private T message;
        private bool returned;

        // Default constructor
        public MessageFuture() : this(default(T)) {
            this.returned = false;
        }

        // Constructor that initializes MessageFuture to point
        // to Message immediately.
        public MessageFuture(T message) {
            this.message = message;
        }

        // Constructor that will attempt to cast an object
        // to the defined future representation type.
        public MessageFuture(object message) {
            this.message = (T)message;
        }

        // Assignment operator that binds this and future
        // to the same future representation (return value type)
        // which is created if necessary
        public void SetFuture(T rep) {
            this.message = rep;
            this.returned = true;
        }

        // Blocks waiting to obtain the result of
        // the asynchronous method invocation
        public T Wait() {
            while (!this.returned) {
            }
            return this.message;
        }
    }
}

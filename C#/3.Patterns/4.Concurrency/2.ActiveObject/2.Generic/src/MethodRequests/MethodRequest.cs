﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Nothing changed with this interface.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    interface MethodRequest {
        // Evaluate the synchroniation constraint.
        bool Guard();

        // Implement the method.
        void Call();
    }
}

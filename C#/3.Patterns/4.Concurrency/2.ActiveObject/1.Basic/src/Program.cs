﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Defines an interface of the ActiveObject pattern.
 * Decouples method call from execution in a separate thread or thread pool.
 * Method invocation is made on an on client thread and method execution
 * is performed by an independent thread asynchronously without blocking the
 * client thread.
 * 
 * Six Elements:
 * 1. Proxy - Wraps real object away and separates the synchronization logic
 *      from the operating logic of the class.
 * 2. Command Pattern - For wrapping and providing an interface to
 *      real object operations.
 * 3. Producer/Consumer - Thread of execution consumes method calls
 *      and dictates when to call them from the producing client.
 * 4. Scheduler - Determines the order of execution
 * 5. Real Object - The class which must be operating asynchronously.
 * 6. Message_Future - Callback or variable for client to receive the result.
 *      Also provides an interface to determine if this call is blocking.
 * 
 * https://en.wikipedia.org/wiki/Active_object
 * Please see the Schmidt paper as well for more information.
 * 
 * An active object example. When you need 1-class per process.
 * This is the simplest form. More modern variants add more flexibility
 * in variants or a command pattern.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class Program {
        static void Main(string[] args) {
            MsgQProxy msgQ = new MsgQProxy(); // Create active object

            msgQ.Put("Hello"); // Use just like a real object.

            Console.ReadKey(); // Wait for user to proceed.

            Console.WriteLine(msgQ.Get()); // Get the value we just stored

            Environment.Exit(0); // Close terminal.
        }
    }
}

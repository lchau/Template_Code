﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Actual representation of a return value
 * which comes back at some later point in time
 * from the method request invocation.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class MessageFuture {
        private string message;
        private bool returned;

        // Default onstructor
        public MessageFuture() : this("") {
            this.returned = false;
        }

        // Constructor that initializes MessageFuture to point
        // to Message immediately.
        public MessageFuture(string message) {
            this.message = message;
        }

        // Assignment operator that binds this and future
        // to the same future representation (return value type)
        // which is created if necessary
        public void SetFuture(string rep) {
            this.message = rep;
            this.returned = true;
        }

        // Blocks waiting to obtain the result of
        // the asynchronous method invocation
        public string Wait() {
            while (!this.returned) {
            }
            return this.message;
        }
    }
}

﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Command pattern. All types of method requests
 * that are accepted by the activation queue inherit
 * from this.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    interface MethodRequest {
        // Evaluate the synchroniation constraint.
        bool Guard();

        // Implement the method.
        void Call();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class Put : MethodRequest {
        private MsgQServant msgQ_Servant;
        private string message;

        public Put(MsgQServant servant, string msg) {
            msgQ_Servant = servant;
            message = msg;
        }

        // Syncronization constraint: only allow
        // Put() calls when the queue is not full.
        public bool Guard() {
            return !this.msgQ_Servant.isFull();
        }

        public void Call() {
            this.msgQ_Servant.Put(message);
        }
    }
}

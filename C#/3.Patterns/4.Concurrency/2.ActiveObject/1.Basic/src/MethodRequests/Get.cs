﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class Get : MethodRequest {
        private MsgQServant msgQ_Servant;
        private MessageFuture result;

        public Get(MsgQServant rep, MessageFuture f) {
            this.msgQ_Servant = rep;
            this.result = f;
        }

        public bool Guard() {
            // Synchronization constraint:
            // cannot call a Get() method until
            // the queue is not empty.
            return !this.msgQ_Servant.isEmpty();
        }

        public void Call() {
            // Bind the dequeued message to the
            // future result object.
            this.result.SetFuture(msgQ_Servant.Get());
        }
    }
}

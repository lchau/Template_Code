﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * A proxy between the client and the real object.
 * This proxy decouples the synchronization logic from the class.
 * For this example the implementation details concerning the
 * message queue reside in the message queue class. Details about
 * prioritizing and scheduling operations on the class should not
 * be baked into the class itself. (Single-Responsibility Principle)
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActiveObject {
    class MsgQProxy {
        // Bound the message queue size.
        public static int MAX_SIZE = 100;

        private MsgQServant msgQServant;
        private MsgQScheduler msgQScheduler;

        public MsgQProxy () : this(MAX_SIZE) {

        }

        public MsgQProxy (int size) {
            this.msgQServant = new MsgQServant(size);
            this.msgQScheduler = new MsgQScheduler(size);
        }

        // Schedule Put Command to execute on the active object
        public void Put(string message) {
            MethodRequest MethodRequest = new Put(this.msgQServant, message);
            msgQScheduler.Enqueue(MethodRequest);
        }

        // Return a Message_Future as the "future"
        // result of an asychronous "get"
        // method on the active object. (Return value)
        public string Get() {
            MessageFuture result = new MessageFuture();

            MethodRequest methodRequest = new Get(msgQServant, result);
            this.msgQScheduler.Enqueue(methodRequest);
            return result.Wait();
        }
    }
}

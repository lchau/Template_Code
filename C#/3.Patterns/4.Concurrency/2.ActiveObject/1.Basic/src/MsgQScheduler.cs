﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Decides which method request to dequeue next and execute
 * on the servant that implements this method. For more
 * information see
 * https://en.wikipedia.org/wiki/Scheduling_(computing)
 * 
 * This is the classical producer-consumer problem.
 */

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;

namespace ActiveObject {
    class MsgQScheduler {

         // Maintains a bounded buffer of pending method
         // requests created by the proxy. This queue keeps
         // track of which method requests to execute. Also
         // decouples the client thread from the servant
         // thread so the two threads can run concurrently.
        private BlockingCollection<MethodRequest> actQueue;
        private Thread thread;

        // Initialize the Activation_Queue to have the
        // specified capacity and make the Scheduler
        // run in its own thread of control.
        public MsgQScheduler(int high_water_mark) {
            this.actQueue
                = new BlockingCollection<MethodRequest>(new ConcurrentBag<MethodRequest>(), high_water_mark);

            // Spawn a separate thread to dispatch method requests.
            this.thread = new Thread(this.Dispatch);
            this.thread.Start();
        }

        ~MsgQScheduler() {
            this.thread.Abort();
        }

        // Other constructors/destructors etc.

        // Insert the Method_Request into the
        // Activation_Queue. This method runs
        // in the thread of its client, i.e.,
        // in the Proxy's thread.
        public void Enqueue(MethodRequest methodRequest) {
            // We always want to keep 1 item in the queue just in case
            // we are inspecting one and need to re-add so we don't end up
            // blocking forever in the thread.
            if (actQueue.Count >= actQueue.BoundedCapacity - 1) 
                throw new ConstraintException("Activation Queue is full");
            if (!actQueue.TryAdd(methodRequest)) 
                throw new InvalidOperationException("Could not add method request to activation queue");
        }

        // Dispatch the Method_Request's on their Servant
        // in the Scheduler's thread.
        public void Dispatch() {
            // Iterate continuously in a separate thread
            while (true) {
                for (int i = actQueue.Count-1; i >= 0; i--) {
                    MethodRequest methodRequest;
                    bool removed = actQueue.TryTake(out methodRequest);

                    // Select a method request whose guard evaluates to true
                    if (methodRequest.Guard() && removed) methodRequest.Call();
                    else actQueue.Add(methodRequest); // Re-add if guard isn't working.
                }
            }
        }
    }
}

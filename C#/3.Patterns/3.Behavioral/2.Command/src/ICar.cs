﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Header or interface for car.
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proxy {
    interface ICar {
        void DriveCar();
    }
}

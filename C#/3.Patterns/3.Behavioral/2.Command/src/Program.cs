﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Date: 02/06/2018
 * Command pattern example.
 * The command pattern is a behavioral design pattern in which
 * an object is used to represent a command, and encapsulate
 * all the information needed (method, params) needed
 * to perform an action at a later time.
 * 
 * Reasons to use:
 *  Need to issue requests to objects without knowing about the
 *    operation being requested or the receiver of the request.
 * 
 * https://en.wikipedia.org/wiki/Command_pattern
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// Client that creates the invoker to use.
namespace Command {
    class Program {
        static void Main(string[] args) {
            string argument = args.Length > 0 ? args[0].ToUpper() : Console.ReadLine();

            ISwitchable lamp = new Light();
            
            // Pass reference to lamp instance to each command (Dependency Injection)
            ICommand switchClose = new CloseSwitchCommand(lamp);
            ICommand switchOpen = new OpenSwitchCommand(lamp);

            // Pass reference to instances of the Command objs to the switch
            Switch @switch = new Switch(switchClose, switchOpen);

            if (argument == "ON") {
                // Switch (invoker) will invoke Execute() on the command obj.
                @switch.Close();
            } else if (argument == "OFF") {
                // Switch (invoker) will invoke Execute() on the command obj.
                @switch.Open();
            } else {
                Console.WriteLine("Argument \"ON\" or \"OFF\" is required.");
            }

            Console.ReadKey();
        }
    }
}

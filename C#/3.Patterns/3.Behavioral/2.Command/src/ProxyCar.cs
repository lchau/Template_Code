﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Proxy class pattern implementation to separate
 * the actual logic of driving a car from the validating
 * operation of checking the driver's age.
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proxy {
    class ProxyCar : ICar {
        private Driver driver;
        private ICar realCar;

        public ProxyCar(Driver driver) {
            this.driver = driver;
            this.realCar = new Car();
        }

        public void DriveCar() {
            if (driver.Age < 16)
                Console.WriteLine("Sorry, the driver is too young to drive.");
            else
                this.realCar.DriveCar();
        }
    }
}

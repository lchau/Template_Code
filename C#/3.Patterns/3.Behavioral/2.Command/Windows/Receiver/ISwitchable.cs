﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Command {
    interface ISwitchable {
        void PowerOn();
        void PowerOff();
    }
}

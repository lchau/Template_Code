﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Date: 02/06/2018
 * The receiver class which is the class being
 * executed.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Command {
    class Light : ISwitchable {

        public void PowerOn() {
            Console.WriteLine("The light is on");
        }

        public void PowerOff() {
            Console.WriteLine("The light is off");
        }
    }
}

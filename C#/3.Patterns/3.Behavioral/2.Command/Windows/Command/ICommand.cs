﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Date: 02/06/2018
 * A command interface is used by client/invoker
 * to execute the command without knowing anything about
 * the command implementation or creation details.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// This is the main characteristic of the command pattern
// an interface used to define objects that represent operations.
namespace Command {
    interface ICommand {
        void Execute();
    }
}

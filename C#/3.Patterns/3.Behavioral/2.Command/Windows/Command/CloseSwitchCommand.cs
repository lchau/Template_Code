﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Date: 02/06/2018
 * A command that switches the light on by making
 * a closed circuit.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Command {
    class CloseSwitchCommand : ICommand {
        private ISwitchable switchable;

        // Pass the object to be acted on
        public CloseSwitchCommand(ISwitchable switchable) {
            this.switchable = switchable;
        }

        // Obfuscate the actual call.
        public void Execute() {
            switchable.PowerOn();
        }
    }
}

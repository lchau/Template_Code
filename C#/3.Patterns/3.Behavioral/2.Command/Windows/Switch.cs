﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Date: 02/06/2018
 * The invoking class of the command interface.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Command {
    class Switch {
        ICommand closedCommand;
        ICommand openedCommand;

        public Switch(ICommand closedCommand, ICommand openedCommand) {
            this.closedCommand = closedCommand;
            this.openedCommand = openedCommand;
        }

        // Close the circuit/power on
        public void Close() {
            this.closedCommand.Execute();
        }

        // Open the circuit/power off
        public void Open() {
            this.openedCommand.Execute();
        }
    }
}

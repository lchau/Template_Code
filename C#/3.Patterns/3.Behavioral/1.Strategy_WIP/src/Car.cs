﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * Real car object implementation.
 */ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proxy {
    class Car : ICar {
        public void DriveCar() {
            Console.WriteLine("Car is being driven.");
        }
    }
}

﻿/* Author: Larry Chau <LarryMChau@gmail.com>
 * A driver drives a car. Only drivers of a certain age can
 * drive a car.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proxy {
    public class Driver {
        public int Age { get; set; }

        public Driver(int age) {
            this.Age = age;
        }
    }
}

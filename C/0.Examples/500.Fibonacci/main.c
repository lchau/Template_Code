/* Author: Larry Chau <larrymchau@gmail.com>
 * Dynamic programming example of calculating a fibonacci sequence.
 * <http://jlordiales.me/2014/02/20/dynamic-programming-introduction/>
 * Example:
 *   Sequence: 1, 1, 2, 3, 5, 8, 13
 *   Index(n): 1, 2, 3, 4, 5, 6, 7 
 */

/////////////////////////////// Includes //////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>

/////////////////////// Defines / Types / Prototypes //////////////////////////

int main(int argc, char** argv);
static long unsigned _FibonacciNaive(uint16_t n);
static long unsigned _FibonacciRecursive(uint16_t n);
static long unsigned _FibonacciIterative(uint16_t n);
static long unsigned _FibonacciIterativeOptimized(uint16_t n);

///////////////////////////// Private Data ////////////////////////////////////

static long unsigned fib_cache[USHRT_MAX] = {0};

////////////////////////////// Definitions ////////////////////////////////////

int main(int argc, char** argv) 
{
    uint16_t fib_idx = 0;
    long unsigned seq_num = 0;

    if (argc > 1) {
        fib_idx = atoi(argv[1]);
        if (fib_idx > 0) {

            seq_num = _FibonacciIterativeOptimized(fib_idx);

            printf("Fibonacci(%hu) == %lu\n", fib_idx, seq_num);
            
            printf("ULONG_MAX     == %lu\n",ULONG_MAX);
            
            return 0;
        }
    } 

    puts("Usage: ./fibonacci [FIB_IDX] (where FIB_IDX > 0)");

    return 1;
}

static long unsigned _FibonacciNaive(uint16_t n) 
{
    return (n < 3) ? 1 : _FibonacciNaive(n-2) + _FibonacciNaive(n-1);
}


static long unsigned _FibonacciRecursive(uint16_t n)
{
    if (n < 3) return 1;
    if (fib_cache[n-1] != 0) return fib_cache[n-1];

    fib_cache[n-1] = _FibonacciRecursive(n-2) + _FibonacciRecursive(n-1);

    return fib_cache[n-1];
}

static long unsigned _FibonacciIterative(uint16_t n)
{
    fib_cache[0] = 1;
    fib_cache[1] = 1;

    for (int i = 2; i < n; i++) {
        fib_cache[i] = fib_cache[i-2] + fib_cache[i-1];
    }

    return fib_cache[n-1];
}

static long unsigned _FibonacciIterativeOptimized(uint16_t n)
{
    long unsigned current;
    long unsigned n1 = 1;
    long unsigned n2 = 1;

    for (int i = 3; i <= n; i++) {
        current = n1 + n2;
        n2 = n1;
        n1 = current;
    }

    return current;
}

/* Author: Larry Chau <larrymchau@gmail.com>
 * Rudimentary quicksort implementation.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

void swap(char unsigned* a, char unsigned* b)
{
    char unsigned temp = *a;

    *a = *b;
    *b = temp;

    return;
}

int partition(char unsigned* s, int low, int high)
{
    char unsigned pivot = s[high];
    int part_cnt = low; // Index of lower partition
    
    // Iterate until pivot
    for (int j = low; j <= high - 1; j++) {
        if (s[j] <= pivot) { // Store in lower partition
            swap(s + part_cnt, s + j);
            part_cnt++;
        }
    }

    swap(s+part_cnt, s+high);
    return part_cnt; // Return pivot index
}

void QuickSort(char unsigned* s, int low, int high) 
{
    if (low < high) {
        int pi = partition(s, low, high);
        
        QuickSort(s, low, pi-1); // Lower partition before pivot
        QuickSort(s, pi+1, high); // Upper partition after pivot
    }

    return;
}

int main(int argc, char**argv) 
{
    char sortedArray[] =   "0123456789"\
                          "ABCDEFGHIJKLMNOPQRSTUVWXYZ"\
                          "abcdefghijklmnopqrstuvwxyz";
    char unsortedArray[] = "5DcYZIXlPFyom3LqitanKsBjGT"\
                          "bA4k8QOvupHeVxM0USRdNCf7h9"\
                          "wWrzg16J2E";
   
    printf("Input:    %s\n", unsortedArray);

    QuickSort((char unsigned*)unsortedArray, 0, strlen(unsortedArray)-1);

    printf("Result:   %s\n", unsortedArray);
    printf("Expected: %s\n", sortedArray);

    return 0;
}

/* Author: Larry Chau <larrymchau@gmail.com>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int ParseBrackets(char* str, char* start_char, char* end_char, int* start, int* end) {
    *start = -1;
    *end = -1;

    for (int i = 0; i < strlen(str); i++) {
        if (str[i] == *start_char && *start == -1) {
            *start = i+1;
        }

        if (str[i] == *end_char && *start != -1) {
            *end = i;
            return 0;
        }
    }

    return 1;
}

int main(int argc, char**argv) {
    int str_start = -1;
    int str_end = -1;
    int cnt_start = -1;
    int cnt_end = -1;
    int count = 0;
    char* count_str = NULL;
    char* repeat_str = NULL;

    if (argc < 2) return 1;
    repeat_str = argv[1];

    if (ParseBrackets(repeat_str, "(", ")", &str_start, &str_end)) return 1;
    repeat_str = &repeat_str[str_start];

    if (strlen(argv[1]) <= str_end) return 1;

    count_str = &argv[1][str_end+1];
    if (ParseBrackets(count_str, "[", "]", &cnt_start, &cnt_end)) return 1;
    count_str = &count_str[cnt_start];

    argv[1][str_end] = '\0';
    count_str[cnt_end] = '\0';
    count = atoi(count_str);

    for (int i = 0; i < count; i++) {
        puts(repeat_str);
    }

    return 0;
}

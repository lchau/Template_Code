/* Author: Larry Chau <larrymchau@gmail.com>
 */

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

char* GetStringBrackets(char *const str, char const*const start_flag, char const*const end_flag) {
    char* start = NULL;
    char* end = NULL;

    start = strstr(str, start_flag);
    if (!start && strlen(start) == 0) return NULL;
    start++;

    end = strstr(start, end_flag);
    if (!end) return NULL;
    end[0] = '\0';

    return start;
}

int main(int argc, char**argv) {
    char* str = argv[1];
    char* count_str = NULL;
    uint32_t count;

    if(argc < 2) return 1;

    str = GetStringBrackets(str, "(", ")");

    if (!str) return 1;

    count_str = str + strlen(str) + 1;

    if(*count_str == '\0') return 1;

    count_str = GetStringBrackets(count_str, "[", "]");

    if (!count_str) return 1;

    if (strlen(count_str) > 10) return 1;

    count = atoi(count_str);

    for(int i = 0; i < count; i++) {
        puts(str);
    }

    return 0;
}

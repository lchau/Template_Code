/* Author: Larry Chau <larrymchau@gmail.com>
 * Rudimentary binary search implementation.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

int BinarySearch(char unsigned* s, char unsigned value) {
    int left = 0, right = strlen((char*)s)-1;
    int pivot = 0;
    
    while (left <= right) { // Handles size == 0 too.
        pivot = left + ((right - left)/2);
        //printf("pivot:%c,left:%d,right:%d\n",s[pivot],left,right);
        if (value == s[pivot]) {
            return pivot;
        } else if (value > s[pivot]) { // Search right
            left = pivot + 1;
        } else { // Search left
            right = pivot - 1;
        }
    }

    return -1;
}

int main(int argc, char**argv) 
{
    char* sortedArray = "0123456789"\
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"\
                        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < strlen(sortedArray); i++) {
        int find_idx = BinarySearch((char unsigned*)sortedArray, sortedArray[i]);
        if (find_idx == -1) return 1;
        printf("Found %c at index %d\n", sortedArray[find_idx], find_idx);
    }

    return 0;
}

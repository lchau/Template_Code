#include <stdio.h>
#include <stdbool.h>

#define SQUARE(a) a*a

int main(void)
{
    int input_arr[5] = {3, 1, 4, 6, 5};
    int input_arr_len = sizeof(input_arr)/sizeof(int);
    int a,b,c;
    bool is_triplet = false;

    for (int i = 0; i < input_arr_len; i++) {
        for (int j = i+1; j < input_arr_len; j++) {
            a = input_arr[i];
            b = input_arr[j];

            for (int k = 0; k < input_arr_len; k++) {
                if (k == j || k == i) continue;
                c = input_arr[k];
                if (SQUARE(a) + SQUARE(b) == SQUARE(c)) {
                    is_triplet = true;
                    goto end_loop;
                }
            }
        }
    }

    end_loop:;
    if (is_triplet) {
        printf("There is a pythagorean triplet %d^2 + %d^2 = %d^2", a, b, c);
    } else {
        printf("There is no pythagorean triplet");
    }
 
	return 0;
}

/* Author: Larry Chau <larrymchau@gmail.com>
 * Rudimentary hash table implementation
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define TABLE_SZ 100

typedef struct _KV_Pair {
    char* key;
    char* value;    
} KV_Pair;

KV_Pair kv_pairs[TABLE_SZ];

unsigned long HashFunc(unsigned char* str)
{
    unsigned long hash = 5381;
    unsigned char str_cpy[strlen(str)+1];
    unsigned char* str_cpy_ptr = str_cpy;
    int c;
   
    memset(str_cpy,   0, strlen(str)+1); 
    memcpy(str_cpy, str, strlen(str)+1);

    while ((c = *str_cpy_ptr++)) {
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash;
}

int GetHashIdx(unsigned char* key, uint32_t* hash_idx, int check_nul)
{
    unsigned long hash = HashFunc(key);
    uint32_t index = hash % TABLE_SZ;

    for (uint32_t i = 0; i < TABLE_SZ; i++) {
        index = (index + i) % TABLE_SZ;

        if (check_nul && kv_pairs[index].key == NULL) {
            *hash_idx = index;
            return 0;
        }
        if (kv_pairs[index].key == (char*)key) {
            *hash_idx = index;
            return 0;
        }
    }

    return 1;
}

int Put(char* key, char* value)
{
    uint32_t hash_idx = 0;

    if (GetHashIdx(key, &hash_idx, 1)) return 1;

    kv_pairs[hash_idx].key = key;
    kv_pairs[hash_idx].value = value;
    return 0;
}

int Get(char* key, char** value)
{
    uint32_t hash_idx = 0;

    if (GetHashIdx((unsigned char*)key, &hash_idx, 0)) return 1;

    *value = kv_pairs[hash_idx].value;
    return 0;
}

int Remove(char* key, char** value)
{
    uint32_t hash_idx = 0;

    if (GetHashIdx((unsigned char*)key, &hash_idx, 0)) return 1;

    *value = kv_pairs[hash_idx].value;
    kv_pairs[hash_idx].key = NULL;
    kv_pairs[hash_idx].value = NULL;

    return 0;
}


int main(int argc, char**argv) 
{
    char* key_1 = "Larry";
    char* val_1 = "Age:26";
    char* key_2 = "Kent";
    char* val_2 = "Age:27";
    char* val_2_2 = "Age:28";

    memset(kv_pairs, 0, sizeof(KV_Pair)*TABLE_SZ);

    Put(key_1, val_1);
    Put(key_2, val_2);

    char* current_val = NULL;
    Get(key_1, &current_val);
    puts(key_1);
    puts(current_val);
    puts("");

    current_val = NULL;
    Get(key_2, &current_val);
    puts(key_2);
    puts(current_val);
    puts("");

    current_val = NULL;
    Remove(key_1, &current_val);
    puts(key_1);
    puts(current_val);
    puts("");

    current_val = NULL;
    int ret_val = Get(key_1, &current_val);
    puts(key_1);
    printf("%d\n\n", ret_val); 

    Put(key_2, val_2_2);
    current_val = NULL;
    Get(key_2, &current_val);
    puts(key_2);
    puts(current_val);
    puts("");

    return 0;
}

/* Author: Larry Chau <larrymchau@gmail.com>
 * Simple generic hash table implmentation.
 */

#include "hash_table.h"
#include <string.h>

int HashTableInit(Hash_Table const*const hash_table,
     KV_Pair const* kv_pairs, uint32_t const kv_pairs_max,
     HashFunc const*const hash_func) 
{
    hash_table->kv_pairs = kv_pairs;
    hash_table->kv_pairs_max = kv_pairs_max;
    hash_table->hash_func = hash_func;

    memset(hash_table, 0, sizeof(KV_Pair)*kv_pairs_max);

    return 0;
}

int HashTablePut(KV_Pair kv_pair) {

}

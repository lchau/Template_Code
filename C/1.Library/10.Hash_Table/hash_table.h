#ifndef _HASH_TABLE_H_
#define _HASH_TABLE_H_

/////////////////////////////// Includes //////////////////////////////////////
#include <stdint.h>

/////////////////////// Defines / Types / Prototypes //////////////////////////
tyedef unsigned long HashFunc(uint8_t const* key, uint32_t const key_sz);

typedef struct _KV_Pair {
    uint8_t* key;
    void* value;
} KV_Pair;

typedef struct _Hash_Table {
    KV_Pair *const kv_pairs;
    uint32_t kv_pairs_max;
    HashFunc* hash_func;
} Hash_Table;

////////////////////////////// Definitions ////////////////////////////////////

int HashTableInit(Hash_Table const*const hash_table, 
    KV_Pair const* kv_pairs, uint32_t const kv_pairs_max, 
    HashFunc const*const hash_func);
int HashTablePut(uint8_t const* key, uint32_t const key_sz, 
    (void*) value);


#endif // _HASH_TABLE_H_

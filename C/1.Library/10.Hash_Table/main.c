/* Author: Larry Chau <larrymchau@gmail.com>
 * Hash table implementation using void pointers to hold data,
 * but tested for storing strings.
 */

/////////////////////////////// Includes //////////////////////////////////////
#include "hash_table.h"

/////////////////////// Defines / Types / Prototypes //////////////////////////
#define HASH_TABLE_SZ   100

///////////////////////////// Private Data ////////////////////////////////////

/* Build a hash table */
KV_Pair kv_pairs[HASH_TABLE_SZ];
Hash_Table hash_table;
/* End build hash table */

unsigned long ComputeHash(uint8_t const* key, uint32_t const key_sz);

////////////////////////////// Definitions ////////////////////////////////////

// DJB2 hash
unsigned long ComputeHash(uint8_t const* key, uint32_t const key_sz) {
    long unsigned hash = 5381;
    uint32_t key_idx = 0;
    int c = 0;

    while (key_idx < key_sz) {
        c = key[key_idx];
        hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    }

    return hash;
}

int main(void) {
    HashTableInit(&hash_table, kv_pairs, MAX_TABLE_SZ);

    return 0;
}

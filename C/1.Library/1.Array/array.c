/* Author: Larry Chau
 * Simple generic array implementation.
 */

/////////////////////////////// Includes //////////////////////////////////////
#include "array.h"
#include <string.h>
/////////////////////// Defines / Types / Prototypes //////////////////////////
// NONE //

///////////////////////////// Private Data ////////////////////////////////////
// NONE //

////////////////////////////// Definitions ////////////////////////////////////

int ArrayInit(Array *const array, void const*const data, 
    size_t const data_sz, uint32_t const data_cnt_max) 
{
    if (!(array && data)) return 1;

    array->data         = (uint8_t *const) data;
    array->data_sz      = data_sz;
    array->data_cnt_max = data_cnt_max; 

    return 0; 
}

uint32_t ArrayGetCntMax (Array const*const array)
{
    if (!(array && array->data)) return 0;

    return array->data_cnt_max;
}

void* ArrayGetVal(Array const*const array, uint32_t const index) 
{
    if (!(array && array->data && index < (array->data_cnt_max))) {
        return NULL;
    } 

    return (array->data) + index*(array->data_sz);
}

int ArraySetVal(Array const*const array, uint32_t const index, 
    void const*const data)
{
    if (!(array && array->data && index < (array->data_cnt_max))) {
        return 1;
    }

    memcpy(array->data + index*(array->data_sz), data, array->data_sz);

    return 0;
}


int ArrayTerm (Array *const array)
{
    if (!(array && array->data)) {
        return 1;
    }

    array->data         = NULL;
    array->data_sz      = 0;
    array->data_cnt_max = 0;

    return 0;
}

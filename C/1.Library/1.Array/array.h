#ifndef _ARRAY_H_
#define _ARRAY_H_

/////////////////////////////// Includes //////////////////////////////////////
#include <stdint.h>
#include <stddef.h>

/////////////////////// Defines / Types / Prototypes //////////////////////////

typedef struct _Array {
    uint8_t * data;
    size_t    data_sz;
    uint32_t  data_cnt_max;
} Array;

////////////////////////////// Definitions ////////////////////////////////////

int      ArrayInit      (Array *const array, void const*const data, 
                         size_t const data_sz, uint32_t const data_cnt_max);
uint32_t ArrayGetCntMax (Array const*const array);
void*    ArrayGetVal    (Array const*const array, uint32_t const index);
int      ArraySetVal    (Array const*const array, uint32_t const index, 
                         void const*const data);
int      ArrayTerm      (Array *const array);

#endif // _ARRAY_H_

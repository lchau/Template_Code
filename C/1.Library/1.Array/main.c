/* Author: Larry Chau <larrymchau@gmail.com>
 * Simple array implementation with example.
 */

/////////////////////////////// Includes //////////////////////////////////////
#include "array.h"
#include <stdio.h>
#include <string.h>

/////////////////////// Defines / Types / Prototypes //////////////////////////
#define ARRAY_DATA_TYPE char
#define ARRAY_SIZE      100

///////////////////////////// Private Data ////////////////////////////////////

/* Build an array */
ARRAY_DATA_TYPE data[ARRAY_SIZE] = {0};
Array ch_arr;
/* End build array */

////////////////////////////// Definitions ////////////////////////////////////

int main(void) 
{
    char const*const hello_world = "Hello world!";
    char * current_ch = NULL;

    if (ArrayInit(&ch_arr, data, sizeof(ARRAY_DATA_TYPE), ARRAY_SIZE)) {
        return 1;
    }

    // Copy over hello world from string literal.
    for (int i = 0; i < ARRAY_SIZE && i < strlen(hello_world); i++) {
        if (ArraySetVal(&ch_arr, i, hello_world + i)) return 1;
    }

    // Print every value of the array up until we hit null terminator.
    for (int i = 0; i < ArrayGetCntMax(&ch_arr); i++) {
        current_ch = ArrayGetVal(&ch_arr, i);
        if (!current_ch) break;
        printf("%c", *current_ch);
    }
    puts("");

    if (ArrayTerm(&ch_arr)) {
        return 1;
    }

    return 0;
}

#ifndef DEPENDENT_H_ /* Include guard */
#define DEPENDENT_H_

#include "dependee.h"

struct dependent {
  struct dependee dependee; 
};

#endif // DEPENDENT_H_

/* Author: Larry Chau <larrymchau@gmail.com>
 * Proper use of C structs example.
 */

#include <stdio.h>
#include "dependent.h"

int main(void) {
    // automatic allocation, all fields placed on stack
    struct dependent obj;
    obj.dependee.data = "Hello, World!";
    printf("%s\n", obj.dependee.data);
    return 0;
}

/* Author: Larry Chau <larrymchau@gmail.com>
 * Example of how multiple files work together. Entry point has a single
 * created source dependency with a method call to another file
 */

#include <stdio.h>
#include "dependee.h"  /* Include the header here, to obtain the function declaration */

int main(void) {
    char* retVal = function("Hello, World!");  /* Use the function here */
    printf("%s\n", retVal);
    return 0;
}
